//
//  VerihubsCamera.h
//  opencve
//
//  Created by Verihubs on 13/07/20.
//  Copyright © 2020 Verihubs. All rights reserved.
//

#ifndef VerihubsCamera_h
#define VerihubsCamera_h


#ifdef __cplusplus
#import <opencv2/opencv.hpp>
#endif

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@protocol VerihubsCameraDelegate <NSObject>
- (void) changeInstruction: (int) current_instruction instr: (int)instr realPerson: (int)realPerson;
- (void) swiftSaveImg:(UIImage*) image current_instruction:(int)current_instruction;
- (void) attributesDetected:(int) aStatus;
- (void) animation: (int) counter;
- (void) stopTimerFromMM;
@end

// Public interface for camera. ViewController only needs to init, start and stop.
@interface VerihubsCamera : NSObject

-(id) initWithController: (UIViewController<VerihubsCameraDelegate>*)c andImageView: (UIImageView*)iv attributesCheck: (bool*)ac timeout:(int)tiout frameRed:(int)frameRed2 frameGreen:(int)frameGreen2 frameBlue:(int)frameBlue2 timerRed:(int)timerRed2 timerGreen:(int)timerGreen2 timerBlue:(int)timerBlue2 eyeRed:(int)eyeRed2 eyeGreen:(int)eyeGreen2 eyeBlue:(int)eyeBlue2 shape:(int)shape2 blurOpacity:(double)blurOpacity2 timerTime:(int)timerTime2 spoofThreshold:(double)spoofThreshold2 blurThreshold:(double)blurThreshold2 darkThreshold:(double)darkThreshold2;
-(void)setInstruction: (int)total_instruction instruction:(int*) instruction;
-(void)setDimension: (int)sw sh: (int)sh;
-(void)setLightMode: (bool)mode;
-(void)start: (int)delay;
-(void)stop;
-(NSString*)logs;
#ifdef __cplusplus
-(UIImage*)UIImageFromMat2: (cv::Mat)image;
#endif
@end


#endif /* VerihubsCamera_h */
