import Foundation
import verihubs
import UIKit

@objc(VerihubsIosWrapper)
public class VerihubsIosWrapper :  CDVPlugin , VerihubsDelegate{

  var verisdk: VerihubsSDK!
  var resp: String!
  var instruction_count: Int!
  var timeout: Int!
  var commandId: String!
  var string_parameters: [AnyHashable : Any] = [:]
  var asset_parameters: [AnyHashable : Any] = [:]
  var custom_instructions: [Int32] = []
  var attributes_check: [Bool] = []

    public func setResponse(response_status: String)
    {
        self.resp = response_status
    }
    
  @objc
  func initClass(_ command: CDVInvokedUrlCommand) {

    verisdk = VerihubsSDK()
    var pluginResult:CDVPluginResult

    pluginResult = CDVPluginResult.init(status: CDVCommandStatus_OK, messageAs: "Class has been initialized")
    self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
  }


  @objc
  func verifyLiveness(_ command: CDVInvokedUrlCommand) {

    self.instruction_count = command.arguments[0] as! Int?
    self.timeout = command.argument(at: 1) as! Int?
    self.attributes_check = command.argument(at: 3) as! [Bool]
    self.custom_instructions = command.argument(at: 2) as! [Int32]
    self.commandId = command.callbackId
    string_parameters = ["close_eyes":"Kedipkan mata 2 kali",
             "open_mouth":"Buka mulut",
             "tilt_left":"Miringkan kepala ke kiri",
             "tilt_right":"Miringkan kepala ke kanan",
             "see_below":"Tundukkan kepala ke bawah",
             "see_above":"Arahkan wajah ke atas",
             "see_left":"Tengok ke kiri",
             "see_right":"Tengok ke kanan",
             "see_straight":"Arahkan wajah ke depan",
             "capture_alert":"Kami akan mengambil foto tampak depan",
             "follow_instruction":"Posisikan wajah ke frame..",
             "remove_mask":"Mohon lepaskan masker",
             "remove_sunglasses":"Mohon lepaskan kacamata"]

    asset_parameters = ["close_eyes":"vira_head_look_straight",
                      "open_mouth":"vira_mouth_open",
                      "tilt_left":"vira_head_tilt_left",
                      "tilt_right":"vira_head_tilt_right",
                      "see_below":"vira_head_look_down",
                      "see_above":"vira_head_look_up",
                      "see_left":"vira_head_look_left",
                      "see_right":"vira_head_look_right",
                      "see_straight":"vira_head_look_straight",
                      "gif":"vira_head_look_straight",
                      "exit_button":"close_white_bca"]


    verisdk.setFrameColor(red: 255, green: 255, blue: 255)
    verisdk.setTimerColor(red: 93, green: 226, blue: 60)
    
    verisdk.setEyeColor(red: 255, green: 255, blue: 255)
    verisdk.setAssetObject(object: asset_parameters)
    verisdk.setCaptureTextColor(red: 255, green: 255, blue: 255)
    
    verisdk.setFontSize(followTextSize: 22,
                        instructionTextSize: 18,
                        captureTextSize: 18,
                        alertTextSize: 22)
    
    verisdk.setTimerDuration(time: 0)

    print("instruction_count: " + String(self.instruction_count))
    
    verisdk.setThresholdValue(spoof: 75, blur: 50, dark: 50)


    verisdk.verifyLiveness(viewController:self.viewController, delegate:self, instruction_count: self.instruction_count, custom_instructions: self.custom_instructions, attributes_check: self.attributes_check, timeout: self.timeout, string_parameters: self.string_parameters, shape: 2, blurOpacity: 0.8)

  }

  public func onActivityResult(response_result: [AnyHashable : Any])
  {
      var pluginResult:CDVPluginResult
      pluginResult = CDVPluginResult.init(status: CDVCommandStatus_OK, messageAs: response_result)
      self.commandDelegate.send(pluginResult, callbackId: self.commandId)

  }

  @objc
  func getVersion(_ command: CDVInvokedUrlCommand) {

    var response_result: [AnyHashable : Any] = [:]

    let temp2 = ["version": "1.7.1"] as [AnyHashable : Any]

    response_result = Array(response_result.keys).reduce(temp2) { (dict, key) -> [AnyHashable:Any] in
        var dict = dict
        dict[key] = response_result[key]
        return dict
    }
    
    var pluginResult:CDVPluginResult
    pluginResult = CDVPluginResult.init(status: CDVCommandStatus_OK, messageAs: response_result)
    self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
  }
}
